/**
 * MainActivity.java
 * Implements the app's main activity
 * The main activity sets up the main view end inflates a menu bar menu
 * <p>
 * This file is part of
 * TRANSISTOR - Radio App for Android
 * <p>
 * Copyright (c) 2015-16 - Y20K.org
 * Licensed under the MIT-License
 * http://opensource.org/licenses/MIT
 */

package com.elsalvadorradios.kduo;

import android.annotation.TargetApi;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.elsalvadorradios.kduo.helpers.LogHelper;
import com.elsalvadorradios.kduo.helpers.NetworkUtils;
import com.elsalvadorradios.kduo.helpers.SleepTimerService;
import com.elsalvadorradios.kduo.helpers.TransistorKeys;


import org.jsoup.Jsoup;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


/**
 * MainActivity class
 */
public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    /* Define log tag */
    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    public android.support.v7.app.ActionBar actionBar;
    /* Main class variables */
    private View mContainer;
    private BroadcastReceiver mCollectionChangedReceiver;
    private BroadcastReceiver mSleepTimerStartedReceiver;
    //    Drawer variables
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private boolean mSleepTimerRunning;
    private SleepTimerService mSleepTimerService;
    private String mSleepTimerNotificationMessage;
    private Snackbar mSleepTimerNotification;
    private boolean mPlayback;
    TextView tvAppVersion;
    String currentVersion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        actionBar = getSupportActionBar();
        String version = getString(R.string.version) + " " + BuildConfig.VERSION_NAME;


        if (NetworkUtils.isNetworkConnected(getApplicationContext())) {

            setContentView(R.layout.activity_main);

            new GetVersionCode().execute();

            tvAppVersion = (TextView) findViewById(R.id.tvAppVersion);
            tvAppVersion.setText(version);
            currentVersion = BuildConfig.VERSION_NAME;

            // initialize broadcast receivers
            initializeBroadcastReceivers();
            // get notification message
            mSleepTimerNotificationMessage = getResources().getString(R.string.snackbar_message_timer_set) + " ";

            // initiate sleep timer service
            mSleepTimerService = new SleepTimerService();
            navigationView = (NavigationView) findViewById(R.id.navigationView);

            navigationView.setNavigationItemSelectedListener(this);

            drawer = (DrawerLayout) findViewById(R.id.drawerView);

            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
                actionBar.setHomeAsUpIndicator(R.drawable.nav);
            }

        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("No Internet Connection");
            builder.setMessage("Please turn on your internet connection to continue");
            builder.setCancelable(false);
            builder.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), 0);
                    finish();

                }


            });

            builder.show();        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        saveAppState(this);

        if (MainActivityFragment.mCollectionAdapter != null) {
            MainActivityFragment.mCollectionAdapter.notifyDataSetChanged();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterBroadcastReceivers();
    }



    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // activity opened for second time set intent to new intent
        setIntent(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (drawer.isDrawerOpen(Gravity.START)) {
                    drawer.closeDrawer(Gravity.START);
                } else {
                    drawer.openDrawer(Gravity.START);
                }

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_actionbar, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // make sure that MainActivityFragment's onActivityResult() gets called
        super.onActivityResult(requestCode, resultCode, data);
    }


    /* Checks if two-pane mode can be used */
    private boolean detectTwoPane() {
        mContainer = findViewById(R.id.player_container);

        // if player_container is present two-pane layout can be used
        if (mContainer != null) {
            LogHelper.v(LOG_TAG, "Large screen detected. Choosing two pane layout.");
            return true;
        } else {
            LogHelper.v(LOG_TAG, "Small screen detected. Choosing single pane layout.");
            return false;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        android.app.Fragment fragment = getFragmentManager().findFragmentById(R.id.fragment_main);
        // hand results over to fragment main
        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);


    }


    /* Saves app state to SharedPreferences */
    private void saveAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = settings.edit();
        // editor.putInt(PREF_STATION_ID_SELECTED, mStationID);
        editor.putBoolean(TransistorKeys.PREF_TIMER_RUNNING, mSleepTimerRunning);
        editor.apply();
    }


    /* Unregisters broadcast receivers */
    private void unregisterBroadcastReceivers() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mCollectionChangedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mSleepTimerStartedReceiver);

    }


    /* Initializes broadcast receivers for onCreate */
    private void initializeBroadcastReceivers() {
        // RECEIVER: station added, deleted, or changed
        mCollectionChangedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mContainer.setVisibility(View.VISIBLE);
            }
        };
        IntentFilter collectionChangedIntentFilter = new IntentFilter(TransistorKeys.ACTION_COLLECTION_CHANGED);
        LocalBroadcastManager.getInstance(this).registerReceiver(mCollectionChangedReceiver, collectionChangedIntentFilter);

        // RECEIVER: sleep timer service sends updates
        mSleepTimerStartedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // get duration from intent
                long remaining = intent.getLongExtra(TransistorKeys.EXTRA_TIMER_REMAINING, 0);
                if (mSleepTimerNotification != null && remaining > 0) {
                    // update existing notification
                    mSleepTimerNotification.setText(mSleepTimerNotificationMessage + getReadableTime(remaining));
                } else if (mSleepTimerNotification != null) {
                    // cancel notification
                    mSleepTimerNotification.dismiss();
                    // save state and update user interface
                    mPlayback = false;
                    mSleepTimerRunning = false;
                    saveAppState(getApplicationContext());
                }

            }
        };
        IntentFilter sleepTimerIntentFilter = new IntentFilter(TransistorKeys.ACTION_TIMER_RUNNING);
        LocalBroadcastManager.getInstance(this).registerReceiver(mSleepTimerStartedReceiver, sleepTimerIntentFilter);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {


        switch (item.getItemId()) {
            case R.id.navItemSleepTimer:

                final Calendar now = Calendar.getInstance();


                final TimePickerDialog timePickerDialog = new TimePickerDialog(this, android.R.style.Theme_Holo_Light_Dialog, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

                        String chosenTime = convertTo12Hour(hourOfDay + "", minute + "");
                        String currentTime = convertTo12Hour(now.get(Calendar.HOUR_OF_DAY) + "", now.get(Calendar.MINUTE) + "");

                        if (checkTimeDifference(chosenTime, currentTime)) {
                            Toast.makeText(getApplicationContext(), "Selected time must be greater current time! ", Toast.LENGTH_SHORT).show();

                        } else {

                            Calendar thatTime = Calendar.getInstance();
                            thatTime.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH));
                            thatTime.set(Calendar.MONTH, now.get(Calendar.MONTH)); // 0-11 so 1 less
                            thatTime.set(Calendar.YEAR, now.get(Calendar.YEAR));
                            thatTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
                            thatTime.set(Calendar.MINUTE, minute);

                            Calendar today = Calendar.getInstance();

                            long timeDifference = thatTime.getTimeInMillis() - today.getTimeInMillis();
                            handleMenuSleepTimerClick(timeDifference);
                        }


                    }
                }, now.get(Calendar.HOUR_OF_DAY), now.get(Calendar.MINUTE), false);
                timePickerDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                timePickerDialog.show();
                drawer.closeDrawer(Gravity.START);


                break;
            case R.id.navItemAbout:

                String aboutTitle = getResources().getString(R.string.header_about);
                // put title and content into intent and start activity
                Intent aboutIntent = new Intent(MainActivity.this, InfosheetActivity.class);
                aboutIntent.putExtra(TransistorKeys.EXTRA_INFOSHEET_TITLE, aboutTitle);
                aboutIntent.putExtra(TransistorKeys.EXTRA_INFOSHEET_CONTENT, TransistorKeys.INFOSHEET_CONTENT_ABOUT);
                startActivity(aboutIntent);
                drawer.closeDrawer(Gravity.START);

                break;
        }
        return false;

    }

    private boolean checkTimeDifference(String chosenTime, String currentTime) {


        DateFormat f1 = new SimpleDateFormat("h:mm a");


        Date __startTime = null;
        Date __endTime = null;
        try {
            __startTime = f1.parse(currentTime);
            __endTime = f1.parse(chosenTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (__startTime != null && __endTime != null) {
            if (__endTime.before(__startTime) || __startTime.equals(__endTime)) {
                return true;
            }
        } else {
            return true;
        }
        return false;

    }

    private String convertTo12Hour(String hourOfDay, String minueOfDay) {


        String selectedDate = "11:00 AM";
        try {
            String s = hourOfDay + ":" + minueOfDay + ":00";
            DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
            Date d = f1.parse(s);
            DateFormat f2 = new SimpleDateFormat("h:mm a");
            selectedDate = f2.format(d).toUpperCase();
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return selectedDate;

    }

    /* Handles tap timer icon in actionbar */
    private void handleMenuSleepTimerClick(long duration) {
        // load app state
        loadAppState(this);

        // CASE: No station is playing, no timer is running
        if (!mPlayback && !mSleepTimerRunning) {
            // unable to start timer
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.toastmessage_timer_start_unable), Toast.LENGTH_SHORT).show();
        }
        // CASE: A station is playing, no sleep timer is running
        else if (mPlayback && !mSleepTimerRunning) {
            startSleepTimer(duration);
            Toast.makeText(this, getResources().getString(R.string.toastmessage_timer_activated), Toast.LENGTH_SHORT).show();
        }
        // CASE: A station is playing, Sleep timer is running
        else if (mPlayback) {
            startSleepTimer(duration);
            Toast.makeText(this, getResources().getString(R.string.toastmessage_timer_duration_increased) + " [+" + getReadableTime(duration) + "]", Toast.LENGTH_SHORT).show();
        }

    }

    /* Starts timer service and notification */
    private void startSleepTimer(long duration) {
        System.out.println(mSleepTimerService + "value");
        // start timer service
        if (mSleepTimerService == null) {
            mSleepTimerService = new SleepTimerService();
        }
        mSleepTimerService.startActionStart(this, duration);

        // show timer notification
        showSleepTimerNotification(duration);
        mSleepTimerRunning = true;
        LogHelper.v(LOG_TAG, "Starting timer service and notification.");
    }

    /* Stops timer service and notification */
    private void stopSleepTimer() {
        // stop timer service
        if (mSleepTimerService != null) {
            mSleepTimerService.startActionStop(this);
        }
        // cancel notification
        if (mSleepTimerNotification != null && mSleepTimerNotification.isShown()) {
            mSleepTimerNotification.dismiss();
        }
        mSleepTimerRunning = false;
        LogHelper.v(LOG_TAG, "Stopping timer service and notification.");
        Toast.makeText(getApplicationContext(), this.getString(R.string.toastmessage_timer_cancelled), Toast.LENGTH_SHORT).show();
    }

    /* Shows notification for a running sleep timer */
    private void showSleepTimerNotification(long remainingTime) {

        // set snackbar message
        String message;
        if (remainingTime > 0) {
            message = mSleepTimerNotificationMessage + getReadableTime(remainingTime);
        } else {
            message = mSleepTimerNotificationMessage;
        }

        // show snackbar
        mSleepTimerNotification = Snackbar.make(navigationView, message, Snackbar.LENGTH_INDEFINITE);
        mSleepTimerNotification.setAction(R.string.dialog_generic_button_cancel, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // stop sleep timer service
                mSleepTimerService.startActionStop(getApplicationContext());
                mSleepTimerRunning = false;
                saveAppState(getApplicationContext());
                // notify user
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.toastmessage_timer_cancelled), Toast.LENGTH_SHORT).show();
                LogHelper.v(LOG_TAG, "Sleep timer cancelled.");
            }
        });
        mSleepTimerNotification.show();

    }

    /* Translates milliseconds into minutes and seconds */
    private String getReadableTime(long remainingTime) {
        return String.format(Locale.getDefault(), "%02d:%02d",
                TimeUnit.MILLISECONDS.toMinutes(remainingTime),
                TimeUnit.MILLISECONDS.toSeconds(remainingTime) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(remainingTime)));
    }

    /* Loads app state from preferences */
    private void loadAppState(Context context) {
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

        mPlayback = settings.getBoolean(TransistorKeys.PREF_PLAYBACK, false);
        mSleepTimerRunning = settings.getBoolean(TransistorKeys.PREF_TIMER_RUNNING, false);
    }


    private class GetVersionCode extends AsyncTask<Void, String, String> {
        @Override
        protected String doInBackground(Void... voids) {

            String newVersion = null;
            try {
                newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=" + getPackageName() + "&hl=it")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get()
                        .select("div.hAyfc:nth-child(4) > span:nth-child(2) > div:nth-child(1) > span:nth-child(1)")
                        .first()
                        .ownText();
                return newVersion;
            } catch (Exception e) {
                return newVersion;
            }
        }

        @Override
        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            try {
                if (onlineVersion != null && !onlineVersion.isEmpty()) {
                    if (Float.valueOf(currentVersion.replace(".", "")) < Float.valueOf(onlineVersion.replace(".", ""))) {
                        final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                        alertDialog.setTitle(getResources().getString(R.string.application_name));

                        alertDialog.setMessage("New update found.");
                        alertDialog.setCancelable(false);
                        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (android.content.ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        }
//                                    finish();
                                    }
                                });
                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();

                                    }
                                });
                        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
                            @Override
                            public void onShow(DialogInterface arg0) {
                                alertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.primary));
                                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(ContextCompat.getColor(MainActivity.this, R.color.primary));
                            }
                        });

                        alertDialog.show();
                    }
                }
            } catch (Exception e) {
            }
        }

    }


}